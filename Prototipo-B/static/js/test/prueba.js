
describe('internal_FixTheRanking', function() {
  /*Con esta función se comprueba la gestión del ranking de jugadores y que cada puesto corresponda con su equivalente,a sí como los valores no admitidos.*/
  it ('en caso de error, corrije el ranking', function() {
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(14),'ace');
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(13),'king');
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(12),'queen');
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(11),'jack');
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(10),10);
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(9),9);
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(8),8);
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(7),7);
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(6),6);
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheRanking(0),alert('Unknown rank ' + 0));
    });


  });
});

describe('internal_FixTheSuiting', function() {
  /*Con esta función se comprueba el palo de las cartas, y que el ajuste de las mismas sea correcto y funcione como el debido, retornando el nombre correspondiente y 
  gestionando el error en caso de que haya. */
  it ('en caso de error, corrije el palo de la carta', function() {
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheSuiting('c'),'clubs');
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheSuiting('d'),'diamonds');
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheSuiting('h'),'hearts');
    });
    it('debe retornar -1 cuando el valor no esta presente', function() {
      assert.equal(gui_if.internal_FixTheSuiting('s'),'spades');
    });

  });
});


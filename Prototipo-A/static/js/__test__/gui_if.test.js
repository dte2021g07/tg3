/* gui_if.test.js 
@autor grupo 07
*/

const {
    internal_FixTheRanking,     //método importado de la clase gui_if.js
    internal_FixTheSuiting,     //método importado de la clase gui_if.js
} = require('../gui_if')


/* Función describe (método, entrada, salida)
OBJ: Comprobar que las cartas sean corregidas correctamente: ace, king, queen, jack.. (AS, Rey, Caballo, Sota etc..)
REQ: Debe existir un ranking.
*/
describe('Internal_FixTheRanking Test', () => {
    test('Input: 14 --> Output: ace', () => {
        expect(internal_FixTheRanking(14)).toBe('ace')
    })
    test('Input: 13 --> Output: king', () => {
        expect(internal_FixTheRanking(13)).toBe('king')
    })
    test('Input: 12 --> Output: queen', () => {
        expect(internal_FixTheRanking(12)).toBe('queen')
    })
    test('Input: 11 --> Output: acjacke', () => {
        expect(internal_FixTheRanking(11)).toBe('jack')
    })
    test('Input: 10 --> Output: 10', () => {
        expect(internal_FixTheRanking(10)).toBe(10)
    })
    test('Input: 1 --> Output: 1', () => {
        expect(internal_FixTheRanking(1)).toBe(1)
    })
})

/* Función describe (método, entrada, salida)
OBJ: Comprueba que el palo de la carta es el correcto (corazones, espadas, rombos.. etc))
REQ: Debe existir un palo.
*/
describe('Internal_FixTheSuiting Test', () => {
    test('Input: c --> Output: clubs', () => {
        expect(internal_FixTheSuiting("c")).toBe('clubs')
    })
    test('Input: d --> Output: diamonds', () => {
        expect(internal_FixTheSuiting('d')).toBe('diamonds')
    })
    test('Input: h --> Output: hearts', () => {
        expect(internal_FixTheSuiting('h')).toBe('hearts')
    })
    test('Input: s --> Output: spades', () => {
        expect(internal_FixTheSuiting('s')).toBe('spades')
    })
    test('Input: a --> Output: yourself', () => {
        expect(internal_FixTheSuiting('a')).toBe('yourself')
    })
})